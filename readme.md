## A dashboard to extract required fields from a bill using OCR 

Using unet resnet 50 to create a mask of the bill and the required field.
Using tesseract to extract the required fields 
 
- Clone the repo
- Run `pip3 install  -r requirements.txt --user`
- Run `python3 manage.py makemigrations` 
- Run `python3 manage.py migrate`
- Run `python3 manage.py runserver` 
 
