
from django.core.files.temp import NamedTemporaryFile
from django.http import JsonResponse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

import cv2
import io
from PIL import Image
import os 
from base64 import b64decode
import numpy as np 
import tensorflow as tf 
import keras 
from keras.models import model_from_json
from bill_classifier import bill_classify

os.environ['KERAS_BACKEND'] = 'tensorflow'
keras.backend.set_learning_phase(0)
#Initialize Graph
from keras import backend as K
K.clear_session()

graph = tf.get_default_graph()
# Load the model from BillClassify class.
with graph.as_default(): 
	clf = bill_classify.BillClassify()

def index(request):
	return render(request,'bill_classifier/index.html')

import time

@csrf_exempt
def classify(request):
	data = {"success": False}
	if request.method == "POST":
		if request.POST.get("image64", None) is not None:
			# Get the raw data
			base64_data = request.POST.get("image64", None).split(',', 1)[1]
			plain_data = b64decode(base64_data)
			# Convert it into numpy array
			image = np.array(Image.open(io.BytesIO(plain_data)))
			image =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
			#Get the predictions 
			with graph.as_default():
				board = clf.which_board(image,True)
			print(board)
			# Save to dict 
			data["success"] = True 
			data["result"] = board
	return JsonResponse(data)
