import cv2
import imutils

from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder

from keras import layers
from keras.preprocessing import image
from keras.layers import Activation, Conv2D, Flatten, LSTM, Dense, Bidirectional, Input, Dropout, BatchNormalization, CuDNNLSTM, GRU, CuDNNGRU, Embedding, GlobalMaxPooling1D, GlobalAveragePooling1D, MaxPooling2D, AveragePooling2D
from keras.models import Model

import keras.backend as K
from keras.engine.topology import Layer
from keras import initializers, regularizers, constraints
from keras.models import Sequential
from keras import optimizers

from keras.metrics import categorical_accuracy, top_k_categorical_accuracy, categorical_crossentropy
from keras.models import Sequential
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, ModelCheckpoint
from keras.optimizers import Adam
from keras.applications import MobileNet
from keras.applications.mobilenet import preprocess_input
from keras.layers import LeakyReLU
##################################
### Data Augmentation ############
##################################

# Resize the image maintaining the aspect rato 
def resize_image(image, width=256, height = 256, to_gray= False):
#   image = cv2.imread(url)
  if to_gray:
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  if image.shape[1]<image.shape[0]:  
    new_height = height
    new_width  = new_height * image.shape[1] / image.shape[0]
  else: 
    new_width = width
    new_height  = new_width * image.shape[0] / image.shape[1]
  return cv2.resize(image, (int(new_width), int(new_height)))

# Make the image size constant by padding 0's to the image, make image center.
def pad_zeros(image,width=256,height = 256):
  result = np.zeros((height,width,3))
  image = image[:height,:width]
  x,y = int((height -image.shape[0])/2), int((width - image.shape[1])/2) 
  padded = np.pad(image, ((x,x),(y,y),(0,0)), 'constant')
  result[:padded.shape[0],:padded.shape[1]] = padded  
  return result

org = path+'aug_hf/'

def augment(img_url,c,n): 
  img = pad_zeros(resize_image(cv2.imread(img_url)))
  for hf,l in zip([0,0,1,1],[100,150,250,256]):
    image = img[:l]
    for angle in np.arange(0, 90, 30):
      rotated = pad_zeros(resize_image(imutils.rotate_bound(image, angle))) 
      cv2.imwrite(org+ f'rotated_x_{angle}_{n}_{hf}_{c}.png',rotated)

  for hf,l in zip([0,1],[100,256]):
    image = img[:,:l]
    for angle in np.arange(0, 90, 30):
      rotated = pad_zeros(resize_image(imutils.rotate_bound(image, angle))) 
      cv2.imwrite(org+ f'rotated_y_{angle}_{n}_{hf}_{c}.png',rotated)
      
  return c
    
main = []
for n,(url,c) in enumerate(zip(dataset['url'],dataset['class'])):
  print(n)
  main.append(augment(url,c,n))


##################################
### Dataset Preparation ##########
##################################

dataset = pd.DataFrame({'names':os.listdir(path+'aug_hf')})
dataset['class'] = dataset['names'].str.split('.').str[0].str.split('_').str[-2:].str.join('_')
dataset['url'] = path+dataset['names']
dataset['class'].value_counts()
dummies = dataset['class'].str.get_dummies()
dataset = pd.concat([dataset,dummies],1)
  



from sklearn.model_selection import train_test_split 
X_tr,X_val= train_test_split(dataset,stratify = dataset['class'],test_size=0.25)

from keras.preprocessing.image import ImageDataGenerator

datagen = ImageDataGenerator(rescale=1./255)
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = datagen.flow_from_dataframe(
        dataframe=X_tr,
        directory=path+'aug_hf/',
        x_col="names",
        y_col=dummies.columns.values,
        target_size=(256, 256),
        batch_size=16,
        class_mode='categorical')


val_generator = test_datagen.flow_from_dataframe(
        dataframe=X_val,
        directory=path+'aug_hf/',
        x_col="names",
        y_col=dummies.columns.tolist(),
        target_size=(256, 256),
        batch_size=16,
        class_mode='categorical')


##################################
### Modeling          ############
##################################

def get_model():
    model = Sequential()
    model.add(Conv2D(32, (3, 3), padding='same', input_shape=(256,256,3)))
    model.add(LeakyReLU(0.1))
    
    model.add(Conv2D(32, (3, 3)))
    model.add(LeakyReLU(0.1))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(LeakyReLU(0.1))

    model.add(Conv2D(16, (3, 3), padding='same'))
    model.add(LeakyReLU(0.1))
    
    model.add(Conv2D(16, (3, 3)))
    model.add(LeakyReLU(0.1))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    model.add(Flatten())
    model.add(Dense(512))
    model.add(LeakyReLU(0.1))
    model.add(Dropout(0.5))
    model.add(Dense(64, activation='softmax'))

    model.compile('adam',loss="categorical_crossentropy", metrics=["accuracy"])
    return model
  
model = get_model()

model.fit_generator(train_generator, steps_per_epoch=128, epochs=10,validation_data=val_generator)

##################################
### Model Saving ############
##################################

# serialize model to JSON
model_json = model.to_json()
with open("drive/My Drive/OCR/bill_classifier_model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("drive/My Drive/OCR/bill_classifier_model.h5")

print("Saved model to disk")

  