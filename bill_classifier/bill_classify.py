import argparse
import cv2
import re 
import numpy as np 
import keras_segmentation
import os 
import tensorflow as tf
import pytesseract
import pickle 
import json
from os import path
import time 
from keras.models import model_from_json
import pandas as pd 
from collections import Counter
import sklearn
from PIL import Image
from numpy import nan
from django.core.files.temp import NamedTemporaryFile
import time

class BillClassify:
	def __init__(self):

		self.width = 200
		self.height = 250
	
		self.key_vals = {	1: 'AJMER VIDYUT VITRAN NIGAM LTD', 
							2: 'ANDHRA PRADESH SOUTHERN POWER DISTRIBUTION COMPANY LIMITED', 
							3: 'Adani Electricity', 
							4: 'BANGLORE ELECTRICITY SUPPLY COMPANY LIMITED', 
							5: 'BEST -BrihanMumbai Electricity Supply and Transport ', 
							6: 'BSES RAJDHANI', 
							7: 'BSES YAMUNA (1)', 
							8: 'CHHATTISGARH STATE POWER DISTRIBUTION COMPANY LIMITED', 
							9: 'Calcutta Electric Supply Corporation', 
							10: 'DAKSHIN HARYANA BIJLI VITRAN NIGAM',
							11: 'Dakshin Gujarat Vij Company Limited',
							12: 'IISER Bhopal', 
							13: 'JAIPUR VIDYUT VITRAN NIGAM LTD', 
							14: 'Kerala State Electricity Board', 
							15: 'MADHYA PRADESH POORV KSHETRA VIDYUT VITARAN COMPANY LTD RURAL', 
							16: 'MAHARASHTRA STATE ELECTRICITY BOARD', 
							17: 'Madhyanchal Vidyut Vitaran Nigam Ltd', 
							18: 'NORTH BIHAR POWER DISTRIBUTION COMPANY LIMITED', 
							19: 'ODISHA DISCOM', 
							20: 'PASCHIM GUJARAT VIJ COMPANY LIMITED', 
							21: 'PUNJAB STATE POWER CORPORATION LIMITED', 
							22: 'Pashchimanchal Vidyut Vitaran Nigam Limited', 
							23: 'RAJASTHAN VIDYUT VITRAN NIGAM LTD', 
							24: 'SOUTH BIHAR POWER DISTRIBUTION COMPANY LIMITED', 
							25: 'TATA POWER DELHI DISTRIBUTION LIMITED', 
							26: 'TELANGANA STATE SOUTHERN POWER DISTRIBUTION COMPANY LIMITED', 
							27: 'Telangana State Northern Power Distribution Company Limited', 
							28: 'Torrent Power', 
							29: 'UTTAR GUJARAT VIJ COMPANY LTD', 
							30: 'UTTAR HARYANA BIJLI VITRAN NIGAM', 
							31: 'Uttar Pradesh Jal Vidyut Nigam Limited', 
							32: 'WEST BENGAL STATE ELECTRICITY DISTRIBUTION COMPANY LIMITED'}

		self.classes =  {0: '1', 1: '10', 2: '11', 3: '12', 4: '13', 5: '14', 6: '15', 7: '16', 8: '17', 9: '18', 10: '19', 11: '2', 12: '20', 13: '21', 14: '22', 15: '23', 16: '24', 17: '25', 18: '26', 19: '27', 20: '28', 21: '29', 22: '3', 23: '30', 24: '31', 25: '32', 26: '4', 27: '5',28: '6', 29: '7', 30: '8', 31: '9'}
		
		self.regex = {	1: '[12][0-9]{11}$', 
						2: '[0-9]{13}$', 
						3: nan, 
						4: nan, 
						5: '[0-9\\,]{10}', 
						6: 'ca no(?:[\s]{0,1}[^A-z]{0,}[\s]{0,})[0-9]{9}', 
						7: '[0-9]{9}', 
						8: nan, 
						9: 'customer [i1]{1}d(?:[.\s]{0,}[^A-z][\s]{0,})[0-9]{11}', 
						10: nan, 
						11: '[0-9]{5,11}', 
						12: nan, 
						13: '[12][0-9]{11}', 
						14: nan, 
						15: '[0-9]{10}', 
						16: 'consumer no(?:[\s]{0,}[^A-z][\s]{0,})[0-9]{12}|consumer number(?:[\s]{0,}[^A-z][\s]{0,})[0-9]{12}', 
						17: nan, 
						18: nan, 
						19: '[0-9]{12}', 
						20: '[0-9]{5,11}', 
						21: nan, 
						22: nan, 
						23: '[12][0-9]{11}', 
						24: nan, 
						25: '6[0-9]{10,11}$|06[0-9]{9,10}', 
						26: '[0-9]{9}', 
						27: nan, 
						28: '[0-9]{1,15}', 
						29: '[0-9]{5,11}', 
						30: 'account no(?:[\s]{0,1}[^A-z][\s]{0,1})[0-9]{10,12}', 
						31: nan, 
						32: 'consumer id(?:[\s]{0,1}[^A-z][\s]{0,1})[0-9]{9}'}

		self.field = {1: 'K Number', 2: 'Service Number', 3: nan, 4: nan, 5: 'Consumer Number', 6: 'CA Number', 7: 'CA Number', 8: nan, 9: 'Customer Id', 10: nan, 11: 'Consumer Number', 12: nan, 13: 'K Number', 14: nan, 15: 'Consumer Number', 16: 'Consumer Number', 17: nan, 18: nan, 19: 'Consumer Number', 20: 'Consumer Number', 21: nan, 22: nan, 23: 'K Number', 24: nan, 25: 'Contract Account Number', 26: 'Unique Service Number', 27: nan, 28: 'Service Number', 29: 'Consumer Number', 30: 'Account ID', 31: nan, 32: 'Consumer ID'}
		
		json_file = open('classifier_model/models/model.json', 'r')
		loaded_model_json = json_file.read()
		json_file.close()
		
		self.model = model_from_json(loaded_model_json)
		self.model.load_weights("classifier_model/models/model.h5")
		
		with open('classifier_model/models/l_d_classifier.h5','rb') as clf:
			self.lmd = pickle.load(clf)
		
		with open('classifier_model/models/l_d_scaler.h5','rb') as scaler:
			self.scaler = pickle.load(scaler)

	def resize_image(self,image,width=256,height = 256, to_gray= False):	
		''' 
			Resize image without loosing aspect ration
			Args : 
				image(np.array) - image to be resized 
				width(int) - image output width . Default = 256
				height(int) - image output height. Default = 256
				to_gray(boolean) - convert image to gray. Default =  False 
			returns: Scaled vesion of the image of specifed output width and height 
		'''
		if to_gray:
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	 	# Deal with width oriented scaling or height oriented scaling 
		if image.shape[1] < image.shape[0]:  
			new_height = height
			new_width  = new_height * image.shape[1] / image.shape[0]
		else: 
			new_width = width
			new_height  = new_width * image.shape[0] / image.shape[1]
		return cv2.resize(image, (int(new_width), int(new_height)))

	# Make the image size constant by padding 0's to the image 
	def pad_zeros(self,image,width=256,height = 256,to_gray=False):
		''' 
			Padd image with zeros to compensate the output width and height
			Args : 
				image(np.array) - image to be resized 
				width(int) - image output width . Default = 256
				height(int) - image output height. Default = 256
			returns: zero padded vesion of the image of specifed output width and height 
		'''
		result = np.zeros((height,width,3))
		image = image[:height,:width]
		x,y = int((height -image.shape[0])/2), int((width - image.shape[1])/2) 
		padded = np.pad(image, ((x,x),(y,y),(0,0)), 'constant')
		result[:padded.shape[0],:padded.shape[1]] = padded  
		return result

	def prepare_to_classify(self,img,width,height,to_gray=False):	
		'''
			Resize image to classify
			returns : Resized and padded imaeg 
		'''
		res = self.resize_image(img,width=width,height=height,to_gray=to_gray)
		if not to_gray:
			padded_img = self.pad_zeros(res,width=width,height=height,to_gray=to_gray)
			return padded_img
		else:
			return res

	def get_intensity(self,img):
		'Returns intensity of the image'
		# Count the valuse 0-255 
		counts = pd.Series(dict(zip(*np.unique(img,return_counts=True))))
		# Make it a dataframe 
		op = pd.concat([pd.DataFrame(columns=np.arange(0,256)),pd.DataFrame([counts])]).fillna(0)
		# Based on the count data, classify if the image is dark or ok 
		key = self.lmd.predict(self.scaler.transform(op))[0]

		if counts.idxmax()==255:
			thres=255
		else:
			thres = counts[counts.index>75].nlargest(20).index.min()
		print(thres)
		print(key)
		# Return the binary threshold with the light or ok class. 
		if key == '0':
			return 'Too dark',0
		else:
			return 'ok',thres

	def get_board(self,key,for_ocr=False):
		# Board and its completeness
		b_n_c = self.classes[key]
		
		# if b_n_c[0] == '0':
		# 	return "Please upload the complete bill"
		# else : 
		if for_ocr:
			return int(b_n_c)
		else:
			return self.key_vals[int(b_n_c)]

	def process_image_for_ocr(self,img,binary_thresh=100):
	    # TODO : Implement using opencv
	    temp_filename = self.set_image_dpi(img)
	    im_new = self.remove_noise_and_smooth(temp_filename,binary_thresh)
	    return im_new

	def set_image_dpi(self,im):
		'''
			Resizes the image and sets the dpi to 300 
		'''
		IMAGE_SIZE = 2500
		im = Image.fromarray(im)
		im = im.convert("RGB")
		length_x, width_y = im.size
		factor = max(1, int(IMAGE_SIZE / length_x))
		size = factor * length_x, factor * width_y
	    # size = (1800, 1800)
		im_resized = im.resize(size, Image.ANTIALIAS)
		temp_file = NamedTemporaryFile(delete=False, suffix='.jpg')
		temp_filename = temp_file.name
		im_resized.save(temp_filename, dpi=(300, 300))
		return temp_filename

	def image_smoothening(self,img,binary_thresh):
		'''
			Binary thresholding for smoothening. 
		'''
		ret1, th1 = cv2.threshold(img, binary_thresh, 255, cv2.THRESH_BINARY)
		ret2, th2 = cv2.threshold(th1, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
		blur = cv2.GaussianBlur(th2, (1, 1), 0)
		ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
		return th3

	def remove_noise_and_smooth(self,file_name,binary_thresh):
		'''
			Noice reduction using adaptive thresholding and morphology
		'''
		img = cv2.imread(file_name, 0)
		filtered = cv2.adaptiveThreshold(img.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 21,3)
		kernel = np.ones((1, 1), np.uint8)
		opening = cv2.morphologyEx(filtered, cv2.MORPH_OPEN, kernel)
		closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
		img = self.image_smoothening(img,binary_thresh)
		or_image = cv2.bitwise_or(img, closing)
		return or_image		
	    			


		  			
	def which_board(self,image,for_ocr=False):
		'''
			Returns the bill.
		'''
		#Get predictions 
		# img = image	
		# Get the intensity and the binary thresh for smoothing 
		intensity,thresh = self.get_intensity(self.prepare_to_classify(image,100,100,to_gray=True))
		# If intensity is ok do ocr 
		if intensity == 'ok':
			# Preprocess the image for classifcation 
			img_x = self.prepare_to_classify(image,256,256)	
			# Get the board class
			op = self.model.predict(np.array([img_x]))
			# Get the board name 
			board = self.get_board(op.argmax(),for_ocr=False)
			if for_ocr:
				key = self.get_board(op.argmax(),for_ocr=True)
				# cv2.imwrite(img,'')
				img = self.process_image_for_ocr(image,thresh) 
				# print(img.shape) 
				# cv2.imwrite('classifier_model/ex.png',img)
				
				# Get the percentage of 255 values in the image
				z_p = (img==255).all(1).sum()/img.shape[0]				
				if pd.isnull(self.regex[key]):
					return '<b> Unable to find the key </b>'
				else:
					# Split the image into 15 segments and do the ocr on each segment.
					# If the segment holds the required regex stop the iteration 
					seg_size = int(img.shape[0]/15)
					start = time.time()
					for i in range(15):
						# Get the segment 
						tes_img = img[i*seg_size:(i+1)*seg_size]
						# print(tes_img.shape)
						if (z_p > 0.3):
							text = pytesseract.image_to_string(img,lang='eng').lower()
						else: 
							text = pytesseract.image_to_string(tes_img,lang='eng').lower()
							text = re.sub('\n',' ',text)
							# print(text)
						# Remove duplicates from regex patterns 
						res  = list(set(re.findall(self.regex[key],text)))
						# If the regex findings is out 				
						if len(res)!=0 :	
							res = [re.sub('[^0-9]','',re.sub("\d*([^\d\W]+)\d*",'',i)) for i in res]
							res = f'<b>Board :</b> {board} <br/> <b>{self.field[key]}: </b>  {res[0]}'
							print("Tesseract took", time.time() - start, "seconds.")
							return res
						else:
							continue
					print("Tesseract took", time.time() - start, "seconds.")
					res = f'Unable to find the key for {board}'
				# print(res,self.regex[key])
				return res
			else:
				return board,0 
		else:
			# If the intensity is dark let the user know. 
			return intensity