from collections import Counter 
import cv2
import os 
import pandas as pd 
import numpy as np 

import cv2
##################################
### Data preparation #############
##################################
 
def apply_brightness_contrast(input_img, brightness = 255, contrast = 127):
    brightness = map(brightness, 0, 510, -255, 255)
    contrast = map(contrast, 0, 254, -127, 127)
 
    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            highlight = 255
        else:
            shadow = 0
            highlight = 255 + brightness
        alpha_b = (highlight - shadow)/255
        gamma_b = shadow
 
        buf = cv2.addWeighted(input_img, alpha_b, input_img, 0, gamma_b)
    else:
        buf = input_img.copy()
 
    if contrast != 0:
        f = float(131 * (contrast + 127)) / (127 * (131 - contrast))
        alpha_c = f
        gamma_c = 127*(1-f)
 
        buf = cv2.addWeighted(buf, alpha_c, buf, 0, gamma_c)
    return buf
 
def map(x, in_min, in_max, out_min, out_max):
    return int((x-in_min) * (out_max-out_min) / (in_max-in_min) + out_min)
 

bright = 255
contrast = 127

IMAGE_SIZE = 1500
import numpy as np 
def process_image_for_ocr(file_path):
    # TODO : Implement using opencv
    temp_filename = set_image_dpi(file_path)
    im_new = remove_noise_and_smooth(temp_filename)
    return im_new

def set_image_dpi(file_path):
    im = Image.open(file_path)
    im = im.convert("RGB")
    length_x, width_y = im.size
    factor = max(1, int(IMAGE_SIZE / length_x))
    size = factor * length_x, factor * width_y
    # size = (1800, 1800)
    im_resized = im.resize(size, Image.ANTIALIAS)
    temp_file = tempfile.NamedTemporaryFile(delete=False, suffix='.jpg')
    temp_filename = temp_file.name
    im_resized.save(temp_filename, dpi=(300, 300))
    return temp_filename

def image_smoothening(img):
    ret1, th1 = cv2.threshold(img, BINARY_THREHOLD, 255, cv2.THRESH_BINARY)
    ret2, th2 = cv2.threshold(th1, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    blur = cv2.GaussianBlur(th2, (1, 1), 0)
    ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return th3

def remove_noise_and_smooth(img):
#     img = cv2.imread(file_name, 0)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    filtered = cv2.adaptiveThreshold(img.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 41,3)
    kernel = np.ones((1, 1), np.uint8)
    opening = cv2.morphologyEx(filtered, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    img = image_smoothening(img)
    or_image = cv2.bitwise_or(img, closing)
    return or_image
  
#Brightness value range -255 to 255
#Contrast value range -127 to 127

path = 'drive/My Drive/OCR/bill_image_dataset/'
img_urls = [path+i for i in os.listdir(path) if 'png' in i]

for url in img_urls:  
  name = url.split('/')[-1].split('.')[0]
  img=cv2.imread(url)
  print(name)
  # Too Dark
  for i in range(0,6):
    img2 = apply_brightness_contrast(img, brightness = i, contrast = 127)
    cv2.imwrite(f'{path}aug_l_d/b_{i}_{name}_0.png',img2)  
  
  # Managable
  for i in range(6,260,55):
    img2 = apply_brightness_contrast(img, brightness = i, contrast = 127)
    cv2.imwrite(f'{path}aug_l_d/b_{i}_{name}_1.png',img2) 
    
  # Too bright  
  for i in range(485,510,4):
    img2 = apply_brightness_contrast(img, brightness = i, contrast = 127)
    cv2.imwrite(f'{path}aug_l_d/b_{i}_{name}_2.png',img2)  



##################################
### Dataset preparation ##########
##################################
 
path = 'drive/My Drive/OCR/bill_image_dataset/aug_l_d'

imgs = os.listdir(path)

df = pd.DataFrame({'img_name':imgs})
df['url'] = path+'/'+df['img_name']

df['class'] = df['img_name'].str.split('.').str[0].str.split('_').str[-1]
def get_features(image, width=150, height = 150, to_gray= False):
#   image = cv2.imread(url)
  if to_gray:
    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
  if image.shape[1]<image.shape[0]:  
    new_height = height
    new_width  = new_height * image.shape[1] / image.shape[0]
  else: 
    new_width = width
    new_height  = new_width * image.shape[0] / image.shape[1]
  img = cv2.resize(image, (int(new_width), int(new_height)))
  
  return Counter(img.ravel())

feat = []
for i,x in enumerate(df['url'].values):
  if i%100==0:print(i)
  f1 = get_features(cv2.imread(x))
  feat.append(f1)
  
op = pd.DataFrame(columns=np.arange(0,256))

X = pd.concat([op,pd.DataFrame(feat)]).fillna(0)
y = df['class']

##################################
### Modeling ####################
##################################

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
scaler.fit(X)
X[X.columns] = scaler.transform(X)
from sklearn.linear_model import LogisticRegression
clf = LogisticRegression(C=2)
from sklearn.model_selection import train_test_split
X_tr,X_te,y_tr,y_te = train_test_split(X,y,stratify=y)

clf.fit(X_tr,y_tr)
print(clf.score(X_te,y_te))

img = cv2.imread('drive/My Drive/OCR/dl/original/1.png')
op = pd.DataFrame(columns=np.arange(0,256))

op = pd.concat([op,pd.DataFrame([get_features(img,100,100,to_gray=True)])]).fillna(0)
clf.predict(scaler.transform(op))

import pickle
with open('drive/My Drive/OCR/l_d_classifier.h5','wb') as file:
  pickle.dump(clf,file)
  
with open('drive/My Drive/OCR/l_d_scaler.h5','wb') as file:
  pickle.dump(scaler,file)