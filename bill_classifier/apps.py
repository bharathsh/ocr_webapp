from django.apps import AppConfig


class BillClassifierConfig(AppConfig):
    name = 'bill_classifier'
