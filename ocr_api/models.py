from django.db import models

# Create your models here.
class Sign(models.Model):
	bill = models.FileField(upload_to="bill/")
	uploaded_at = models.DateTimeField(auto_now_add=True)
	