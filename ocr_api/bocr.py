import argparse
import cv2
import re 
import numpy as np 
import keras_segmentation
import os 
import tensorflow as tf
import pytesseract
import pickle 
import json
from os import path
import time 
class BillOCR:
	def __init__(self,image_address='',retrain=False,checkpoint='unet_model/'):
		self.image_address = image_address
		self.width = 32*64
		self.height = 32*58
		self.retrain = retrain
		self.checkpoint = checkpoint
		latest_weights = checkpoint+'resnet_unet_1.4'
		assert ( not latest_weights is None ) , "Checkpoint not found."
		self.model = keras_segmentation.models.unet.resnet50_unet(n_classes=3,  input_height=32*24, input_width=32*18,)
		self.model.load_weights(latest_weights)

			
	def resize_image(self,image,width=32*18,height = 32*24, to_gray= False):	
		''' 
			Resize image without loosing aspect ration
			Args : 
				image(np.array) - image to be resized 
				width(int) - image output width . Default = 32*18
				height(int) - image output height. Default = 32*24
				to_gray(boolean) - convert image to gray. Default =  False 
			returns: Scaled vesion of the image of specifed output width and height 
		'''
		if to_gray:
			image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	 	# Deal with width oriented scaling or height oriented scaling 
		if image.shape[1] < image.shape[0]:  
			new_height = height
			new_width  = new_height * image.shape[1] / image.shape[0]
		else: 
			new_width = width
			new_height  = new_width * image.shape[0] / image.shape[1]
		return cv2.resize(image, (int(new_width), int(new_height)))

	# Make the image size constant by padding 0's to the image 
	def pad_zeros(self,image,width=32*18,height = 32*24):
		''' 
			Padd image with zeros to compensate the output width and height
			Args : 
				image(np.array) - image to be resized 
				width(int) - image output width . Default = 32*18
				height(int) - image output height. Default = 32*24
			returns: zero padded vesion of the image of specifed output width and height 
		'''
		result = np.zeros((height,width,3))
		image = image[:height,:width]
		result[:image.shape[0],:image.shape[1]] = image
		return result

	def prepare_for_tesseract(self,img,to_gray):	
		'''
			Resize image to greater height for Tesseract OCR 
			returns : Resized and padded imaeg 
		'''
		res = self.resize_image(img,width=32*58,height=32*64,to_gray=to_gray)
		padded_img = self.pad_zeros(res,width=32*58,height=32*64)
		return padded_img
	
	def get_details(self,image):
		'''
			Returns the k number at the moment.
		'''
		#Get predictions 
		img = image	
		start_time = time.time()
		op = self.model.predict_segmentation(img).astype('float32')
		print("--- %s seconds ---" % (time.time() - start_time))
		op = cv2.cvtColor(op,cv2.COLOR_GRAY2BGR)
		# preapre image for tesseract
		img = self.prepare_for_tesseract(img)	
		op = self.prepare_for_tesseract(op)
		# Replace the mask with the original image, convert to greyscale 
		unmask = cv2.cvtColor(np.where(op==2,img,125).astype('uint8'),cv2.COLOR_BGR2GRAY)		
		# Threshold		
		thresh = cv2.adaptiveThreshold(unmask, 125,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY, 155, 28)
		# cv2.imwrite('/home/niki/J_OCR/tresh.png',thresh)
		# Remove the unwanted single color rows in the beginning 
		final = thresh[min(np.where((thresh!=125).any(1))[0]):max(np.where((thresh!=125).any(1))[0])]
		# Use regex to get the 12 digit number 
		return re.findall('\d{12}',pytesseract.image_to_string(final,config='digits'))
