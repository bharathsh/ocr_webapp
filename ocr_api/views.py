
from django.core.files.temp import NamedTemporaryFile
from django.http import JsonResponse
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

import cv2
import io
from PIL import Image
import os 
from base64 import b64decode
from ocr_api import bocr
import numpy as np 
import tensorflow as tf 
import keras 
os.environ['KERAS_BACKEND'] = 'tensorflow'
keras.backend.set_learning_phase(0)
#Initialize Graph
graph = tf.get_default_graph()
# Load the model from BillOCR class. 
ocr = bocr.BillOCR()

def index(request):
	return render(request,'ocr_api/index.html')

import time

@csrf_exempt
def ocr(request):
	data = {"success": False}
	if request.method == "POST":
		if request.POST.get("image64", None) is not None:
			# Get the raw data
			base64_data = request.POST.get("image64", None).split(',', 1)[1]
			plain_data = b64decode(base64_data)
			# Convert it into numpy array
			image = np.array(Image.open(io.BytesIO(plain_data)))
			image =  cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
			#Get the predictions 
			with graph.as_default():
				k_number = ocr.get_details(image)
			# Save to dict 
			data["success"] = True 
			data["result"] = k_number
	return JsonResponse(data)
