"""ocr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from django.conf.urls import url
import ocr_api 
import bill_classifier
urlpatterns = [
	url(r'^ocr_api/',include('ocr_api.urls')),
    url(r'^bill_classifier/',include('bill_classifier.urls')),
    path('admin/', admin.site.urls),
    url(r'^ocr_api/ocr/$', ocr_api.views.ocr),
    url(r'^bill_classifier/classify/$', bill_classifier.views.classify),

]
